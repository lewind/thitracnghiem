/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Answer;


/**
 *
 * @author Admin
 */
public class AnswerDAO extends GenericDAO{
    
    private final static String TABLE_NAME = "answer";
    private final List<String> fields = Arrays.asList("questionId", "image", "isTrue", "content", "created");
    public AnswerDAO() {
        super(TABLE_NAME);
    }
    /**
     * Trả về tất cả các answer trong cơ sở dữ liệu
     * @return 
     */
    public ArrayList<Answer> getAll(){
        ArrayList<Answer> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL();
            while (rs.next()){
                Answer item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về tất cả các item theo điều kiện
     * @param condition điều kiện
     * @return 
     */
    public ArrayList<Answer> getAll(String condition){
        ArrayList<Answer> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL(condition);
            while (rs.next()){
                Answer item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về answer theo Id
     * @param id
     * @return 
     */
    public Answer get(int id){
        try {
            ResultSet rs = super.GET_BY_ID(id);
            if(rs.next()){
                Answer item = this.convertResultSetToObject(rs);
                return item;
            }else{  
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * thêm answer vào cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean add(Answer item){
        try {
            String values = "?, ?, ?, ?, ?";
            PreparedStatement pstmt = super.ADD(String.join(", ", fields), values);
            pstmt.setInt(1, item.getQuestionId());
            pstmt.setString(2, item.getImage());
            pstmt.setBoolean(3, item.isIsTrue());
            pstmt.setString(4, item.getContent());
            pstmt.setDate(5, item.getCreated());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * chỉnh sửa answer đã tồn tại trong cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean update(Answer item){
        try {
            PreparedStatement pstmt = super.UPDATE(fields, item.getId());
             pstmt.setInt(1, item.getQuestionId());
            pstmt.setString(2, item.getImage());
            pstmt.setBoolean(3, item.isIsTrue());
            pstmt.setString(4, item.getContent());
            pstmt.setDate(5, item.getCreated());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * xóa answer khỏi CSDL
     * @param item
     * @return 
     */
    public boolean delete(Answer item){
        return super.DELETE(item.getId());
    }

    /**
     * xóa answer khỏi CSDL
     * @param id
     * @return 
     */
    public boolean delete(int id){
        return super.DELETE(id);
    }
    
    private Answer convertResultSetToObject(ResultSet rs){
        try {
            Answer item = new Answer(rs.getInt("id"));
            item.setQuestionId(rs.getInt("questionId"));
            item.setImage(rs.getString("image"));
            item.setIsTrue(rs.getBoolean("isTrue"));
            item.setContent(rs.getString("content"));
            item.setCreated(rs.getDate("created"));
            return item;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    public static void main(String[] args) {
        AnswerDAO ad = new AnswerDAO();
//        ArrayList<Account> items = ad.getAll();
//        for(Account acc: items){
//            System.out.println(acc.getName());
//        }
        //Account item = new Account(5, "tucun", "1", AccountRole.STUDENT, AccountStatus.APPROVED, "Lê Ngọc Thắng", "Đại học Bách Khoa", new Date(new java.util.Date().getTime()));
        //System.out.println(ad.delete(item) ? "Thành công": "Thất bại");
       
    }
}
