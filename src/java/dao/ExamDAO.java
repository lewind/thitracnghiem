/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Account;
import model.AccountRole;
import model.AccountStatus;
import model.Exam;


/**
 *
 * @author Admin
 */
public class ExamDAO extends GenericDAO{
    
    private final static String TABLE_NAME = "exam";
    private final List<String> fields = Arrays.asList("name", "difficulty", "created", "createdBy", "subjectId");
    public ExamDAO() {
        super(TABLE_NAME);
    }
    /**
     * Trả về tất cả các exam trong cơ sở dữ liệu
     * @return 
     */
    public ArrayList<Exam> getAll(){
        ArrayList<Exam> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL();
            while (rs.next()){
                Exam item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về tất cả các item theo điều kiện
     * @param condition điều kiện
     * @return 
     */
    public ArrayList<Exam> getAll(String condition){
        ArrayList<Exam> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL(condition);
            while (rs.next()){
                Exam item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về exam theo Id
     * @param id
     * @return 
     */
    public Exam get(int id){
        try {
            ResultSet rs = super.GET_BY_ID(id);
            if(rs.next()){
                Exam item = this.convertResultSetToObject(rs);
                return item;
            }else{  
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * thêm exam vào cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean add(Exam item){
        try {
            String values = "?, ?, ?, ?, ?";
            PreparedStatement pstmt = super.ADD(String.join(", ", fields), values);
            pstmt.setString(1, item.getName());
            pstmt.setInt(2, item.getDifficulty());
            pstmt.setDate(3, item.getCreated());
            pstmt.setInt(4, item.getCreatedBy());
            pstmt.setInt(5, item.getSubjectId());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * chỉnh sửa exam đã tồn tại trong cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean update(Exam item){
        try {
            PreparedStatement pstmt = super.UPDATE(fields, item.getId());
            pstmt.setString(1, item.getName());
            pstmt.setInt(2, item.getDifficulty());
            pstmt.setDate(3, item.getCreated());
            pstmt.setInt(4, item.getCreatedBy());
            pstmt.setInt(5, item.getSubjectId());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * xóa exam khỏi CSDL
     * @param item
     * @return 
     */
    public boolean delete(Exam item){
        return super.DELETE(item.getId());
    }

    /**
     * xóa exam khỏi CSDL
     * @param id
     * @return 
     */
    public boolean delete(int id){
        return super.DELETE(id);
    }
    
    private Exam convertResultSetToObject(ResultSet rs){
        try {
            Exam item = new Exam(rs.getInt("id"));
            item.setName(rs.getString("name"));
            item.setDifficulty(rs.getInt("difficulty"));
            item.setCreated(rs.getDate("created"));
            item.setCreatedBy(rs.getInt("createdBy"));
            item.setSubjectId(rs.getInt("subjectId"));
            return item;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    public static void main(String[] args) {
        ExamDAO ad = new ExamDAO();
//        ArrayList<Account> items = ad.getAll();
//        for(Account acc: items){
//            System.out.println(acc.getName());
//        }
        Account item = new Account(5, "tucun", "1", AccountRole.STUDENT, AccountStatus.APPROVED, "Lê Ngọc Thắng", "Đại học Bách Khoa", new Date(new java.util.Date().getTime()));
        //System.out.println(ad.delete(item) ? "Thành công": "Thất bại");
       
    }
}
