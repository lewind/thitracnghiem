/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Connector;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.AccountRole;
import model.AccountStatus;


/**
 *
 * @author Admin
 */
public class AccountDAO extends GenericDAO{
    
    private final static String TABLE_NAME = "account";
    private final List<String> fields = Arrays.asList("username", "password", "role", "status", "created", "name", "unit");
    public AccountDAO() {
        super(TABLE_NAME);
    }
    
    public Account login(String username, String password){
        try {
            con = Connector.getConnection();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE username = ? and password = ?";
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            if(rs.next()){
                return convertResultSetToObject(rs);
            } else{
                return null;
            }
        } catch (SQLException ex) {
            return null;
        }
    }
    /**
     * Trả về tất cả các account trong cơ sở dữ liệu
     * @return 
     */
    public ArrayList<Account> getAll(){
        ArrayList<Account> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL();
            while (rs.next()){
                Account item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về tất cả các item theo điều kiện
     * @param condition điều kiện
     * @return 
     */
    public ArrayList<Account> getAll(String condition){
        ArrayList<Account> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL(condition);
            while (rs.next()){
                Account item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về account theo Id
     * @param id
     * @return 
     */
    public Account get(int id){
        try {
            ResultSet rs = super.GET_BY_ID(id);
            if(rs.next()){
                Account item = this.convertResultSetToObject(rs);
                return item;
            }else{  
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * thêm account vào cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean add(Account item){
        try {
            String values = "?, ?, ?, ?, ?, ?, ?";
            PreparedStatement pstmt = super.ADD(String.join(", ", fields), values);
            pstmt.setString(1, item.getUsername());
            pstmt.setString(2, item.getPassword());
            pstmt.setString(3, item.getRole());
            pstmt.setString(4, item.getStatus());
            pstmt.setDate(5, item.getCreated());
            pstmt.setString(6, item.getName());
            pstmt.setString(7, item.getUnit());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * chỉnh sửa account đã tồn tại trong cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean update(Account item){
        try {
            PreparedStatement pstmt = super.UPDATE(fields, item.getId());
            pstmt.setString(1, item.getUsername());
            pstmt.setString(2, item.getPassword());
            pstmt.setString(3, item.getRole());
            pstmt.setString(4, item.getStatus());
            pstmt.setDate(5, item.getCreated());
            pstmt.setString(6, item.getName());
            pstmt.setString(7, item.getUnit());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * xóa account khỏi CSDL
     * @param item
     * @return 
     */
    public boolean delete(Account item){
        return super.DELETE(item.getId());
    }

    /**
     * xóa account khỏi CSDL
     * @param id
     * @return 
     */
    public boolean delete(int id){
        return super.DELETE(id);
    }
    
    private Account convertResultSetToObject(ResultSet rs){
        try {
            Account item = new Account(
                    rs.getInt("id"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("role"),
                    rs.getString("status"),
                    rs.getString("name"),
                    rs.getString("unit"),
                    rs.getDate("created")
            );
            return item;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    public static void main(String[] args) {
        AccountDAO ad = new AccountDAO();
//        ArrayList<Account> items = ad.getAll();
//        for(Account acc: items){
//            System.out.println(acc.getName());
//        }
        Account item = new Account(5, "tucun", "1", AccountRole.STUDENT, AccountStatus.APPROVED, "Lê Ngọc Thắng", "Đại học Bách Khoa", new Date(new java.util.Date().getTime()));
        //System.out.println(ad.delete(item) ? "Thành công": "Thất bại");
       
    }
}
