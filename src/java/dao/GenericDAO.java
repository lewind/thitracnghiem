/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Connector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import sun.reflect.annotation.AnnotationParser;

/**
 *
 * @author Admin
 * @param <T>
 */
public abstract class GenericDAO {
    protected final String tableName;
    protected Connection con;

    protected GenericDAO(String tableName) {
        this.tableName = tableName;      
    }
    
    protected ResultSet GET_ALL(){
        String query = "SELECT * FROM " + this.tableName;
        con = Connector.getConnection();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    protected ResultSet GET_ALL(String condition){
        String query = "SELECT * FROM " + this.tableName+ " " + condition;
        con = Connector.getConnection();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    protected ResultSet GET_BY_ID(int id){
        String query = "SELECT * FROM " + this.tableName + " WHERE ID = " + id;
        con = Connector.getConnection();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    protected PreparedStatement ADD(String fields, String values){
        try {
            String query = "INSERT INTO " + this.tableName + " ("+fields+") values ("+values+")";
            con = Connector.getConnection();
            PreparedStatement pstmt = con.prepareStatement(query);
            return pstmt;
        } catch (SQLException ex) {
            return null;
        }
    } 
    protected PreparedStatement UPDATE(List<String> fields, int id){
        try {
            String updateFields = String.join(", ", fields.stream().map(item -> item + " = ?").collect(Collectors.toList()));
            String query = "UPDATE " + this.tableName + " SET "+updateFields +" WHERE id = " + id;
            con = Connector.getConnection();
            PreparedStatement pstmt = con.prepareStatement(query);
            return pstmt;
        } catch (SQLException ex) {
            return null;
        }
    } 
    protected boolean DELETE(int id){
        try {
            String query = "DELETE FROM "+ this.tableName+" WHERE id = ?";
            con = Connector.getConnection();
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, id);
            pstmt.execute();
            return true;
        } catch (SQLException ex) {
            return false;
        } finally {
            CLOSE_CONNECTION();
        }
    }
    
    protected void CLOSE_CONNECTION(){
        Connector.closeConnection(con);
    }
}
