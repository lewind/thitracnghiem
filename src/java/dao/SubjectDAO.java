/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Subject;

/**
 *
 * @author Admin
 */
public class SubjectDAO extends GenericDAO{
    
    private final static String TABLE_NAME = "subject";
    private final List<String> fields = Arrays.asList("name", "created", "createdBy");
    public SubjectDAO() {
        super(TABLE_NAME);
    }
    /**
     * Trả về tất cả các subject trong cơ sở dữ liệu
     * @return 
     */
    public ArrayList<Subject> getAll(){
        ArrayList<Subject> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL();
            while (rs.next()){
                Subject item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về tất cả các item theo điều kiện
     * @param condition điều kiện
     * @return 
     */
    public ArrayList<Subject> getAll(String condition){
        ArrayList<Subject> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL(condition);
            while (rs.next()){
                Subject item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về subject theo Id
     * @param id
     * @return 
     */
    public Subject get(int id){
        try {
            ResultSet rs = super.GET_BY_ID(id);
            if(rs.next()){
                Subject item = this.convertResultSetToObject(rs);
                return item;
            }else{  
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * thêm subject vào cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean add(Subject item){
        try {
            String values = "?, ?, ?";
            PreparedStatement pstmt = super.ADD(String.join(", ", fields), values);
            pstmt.setString(1, item.getName());
            pstmt.setDate(2, item.getCreated());
            pstmt.setInt(3, item.getCreatedBy());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * chỉnh sửa subject đã tồn tại trong cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean update(Subject item){
        try {
            PreparedStatement pstmt = super.UPDATE(fields, item.getId());
            pstmt.setString(1, item.getName());
            pstmt.setDate(2, item.getCreated());
            pstmt.setInt(3, item.getCreatedBy());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * xóa subject khỏi CSDL
     * @param item
     * @return 
     */
    public boolean delete(Subject item){
        return super.DELETE(item.getId());
    }

    /**
     * xóa subject khỏi CSDL
     * @param id
     * @return 
     */
    public boolean delete(int id){
        return super.DELETE(id);
    }
    
    private Subject convertResultSetToObject(ResultSet rs){
        try {
            Subject item = new Subject(rs.getInt("id"));
            item.setName(rs.getString("name"));
            item.setCreated(rs.getDate("created"));
            item.setCreatedBy(rs.getInt("createdBy"));
            return item;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    public static void main(String[] args) {
        SubjectDAO ad = new SubjectDAO();
//        ArrayList<Account> items = ad.getAll();
//        for(Account acc: items){
//            System.out.println(acc.getName());
//        }
        //Account item = new Account(5, "tucun", "1", AccountRole.STUDENT, AccountStatus.APPROVED, "Lê Ngọc Thắng", "Đại học Bách Khoa", new Date(new java.util.Date().getTime()));
        //System.out.println(ad.delete(item) ? "Thành công": "Thất bại");
       
    }
}
