/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Account;
import model.AccountRole;
import model.AccountStatus;
import model.Question;


/**
 *
 * @author Admin
 */
public class QuestionDAO extends GenericDAO{
    
    private final static String TABLE_NAME = "question";
    private final List<String> fields = Arrays.asList("subjectId", "content", "image", "chapter", "difficulty", "created", "createdBy");
    public QuestionDAO() {
        super(TABLE_NAME);
    }
    /**
     * Trả về tất cả các answer trong cơ sở dữ liệu
     * @return 
     */
    public ArrayList<Question> getAll(){
        ArrayList<Question> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL();
            while (rs.next()){
                Question item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về tất cả các item theo điều kiện
     * @param condition điều kiện
     * @return 
     */
    public ArrayList<Question> getAll(String condition){
        ArrayList<Question> items = new ArrayList<>();
        try {
            ResultSet rs = super.GET_ALL(condition);
            while (rs.next()){
                Question item = this.convertResultSetToObject(rs);
                items.add(item);
            }
        } catch (SQLException ex) {
            return new ArrayList<>();
        } finally {
            CLOSE_CONNECTION();
        }
        return items;
    }
    
    /**
     * trả về answer theo Id
     * @param id
     * @return 
     */
    public Question get(int id){
        try {
            ResultSet rs = super.GET_BY_ID(id);
            if(rs.next()){
                Question item = this.convertResultSetToObject(rs);
                return item;
            }else{  
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * thêm answer vào cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean add(Question item){
        try {
            String values = "?, ?, ?, ?, ?, ?, ?";
            PreparedStatement pstmt = super.ADD(String.join(", ", fields), values);
            pstmt.setInt(1, item.getSubjectId());
            pstmt.setString(2, item.getContent());
            pstmt.setString(3, item.getImage());
            pstmt.setString(4, item.getChapter());
            pstmt.setInt(5, item.getDifficulty());
            pstmt.setDate(6, item.getCreated());
            pstmt.setInt(7, item.getCreatedBy());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * chỉnh sửa answer đã tồn tại trong cơ sở dữ liệu
     * @param item
     * @return 
     */
    public boolean update(Question item){
        try {
            PreparedStatement pstmt = super.UPDATE(fields, item.getId());
            pstmt.setInt(1, item.getSubjectId());
            pstmt.setString(2, item.getContent());
            pstmt.setString(3, item.getImage());
            pstmt.setString(4, item.getChapter());
            pstmt.setInt(5, item.getDifficulty());
            pstmt.setDate(6, item.getCreated());
            pstmt.setInt(7, item.getCreatedBy());
            pstmt.execute();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            this.CLOSE_CONNECTION();
        }
    }
    
    /**
     * xóa answer khỏi CSDL
     * @param item
     * @return 
     */
    public boolean delete(Question item){
        return super.DELETE(item.getId());
    }

    /**
     * xóa answer khỏi CSDL
     * @param id
     * @return 
     */
    public boolean delete(int id){
        return super.DELETE(id);
    }
    
    private Question convertResultSetToObject(ResultSet rs){
        try {
            Question item = new Question(rs.getInt("id"));
            item.setContent(rs.getString("content"));
            item.setDifficulty(rs.getInt("difficulty"));
            item.setCreated(rs.getDate("created"));
            item.setCreatedBy(rs.getInt("createdBy"));
            item.setSubjectId(rs.getInt("subjectId"));
            item.setChapter(rs.getString("chapter"));
            item.setImage(rs.getString("image"));
            return item;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    public static void main(String[] args) {
        QuestionDAO ad = new QuestionDAO();
//        ArrayList<Account> items = ad.getAll();
//        for(Account acc: items){
//            System.out.println(acc.getName());
//        }
        //Account item = new Account(5, "tucun", "1", AccountRole.STUDENT, AccountStatus.APPROVED, "Lê Ngọc Thắng", "Đại học Bách Khoa", new Date(new java.util.Date().getTime()));
        //System.out.println(ad.delete(item) ? "Thành công": "Thất bại");
       
    }
}
