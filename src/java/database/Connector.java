/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import dao.AccountDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Connector {
    /**
     * Trả về Connection thực hiện thao tác với CSDL
     * @return 
     */
    public static Connection getConnection(){
        Connection conn;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/quanlythitracnghiem?autoReconnect=true&useSSL=false", "root", "abcde12345-");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
        return conn;
    }
    
    /**
     * Thực hiện đóng Connection
     */
    public static void closeConnection(Connection conn){
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        if(getConnection()==null){
            System.out.println("Kết nối thất bại");
        } else{
            System.out.println("Kết nối thành công");
        }
    }
}
